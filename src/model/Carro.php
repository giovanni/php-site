<?php
require_once "ModelAbstract.php";
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/16/13
 * Time: 3:15 PM 
 */

class Carro extends ModelAbstract{
    /**
     * Procura os carros por nome e retorna um array contendo os dados
     * @param $nome
     * @return array
     */
    public function findByNome($nome){
       $sql = "SELECT * from carros where nome like ?";
       $con = $this->getConnection();
        // Proteje contra SQL Injection
       $statement = $con->prepare($sql);
       $statement->bindValue(1,"%$nome%",PDO::PARAM_STR);

       $statement->execute();
       $array =  $statement->fetchAll();
       echo $statement->queryString;
       return $array;
   }
    public function findAll(){
        $sql = "SELECT * from carros";
        $con = $this->getConnection();

        $result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
} 