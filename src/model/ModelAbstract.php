<?php
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/16/13
 * Time: 3:30 PM 
 */

abstract class ModelAbstract {
    /**
     * @var PDO
     */
    protected $con;
    protected $db = "aula";
    protected $user = "root";
    protected $password = "";
    protected $host = "localhost";

    /**
     * Retorna uma conexão com banco de dados já aberta
     * @return PDO
     */
    protected function getConnection(){
        // A classe PDO é a melhor para conexão com base de dados. Ela abstrai qual base é utilizada, (Mysql, Postgresq, SqlLite, Oracle, etc..)
        // Além de ter uma API melhor projetada
        if(is_null($this->con)){
            $this->con = new PDO("mysql:host=$this->host;dbname=$this->db", $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        }
       return $this->con;
    }

   function __destruct(){
       // Usando PDO esse passo é desnecessário, já que o PDO vai fechar a conexão automaticamente ao ser destruído
      if(!is_null($this->con)){
          $this->con = null;
      }
   }
} 