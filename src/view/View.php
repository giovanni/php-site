<?php
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/15/13
 * Time: 1:34 PM 
 */

interface View {
    function render();
}