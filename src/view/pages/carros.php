<?php
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/15/13
 * Time: 1:55 PM 
 */
?>
<style>

    .car-item {
        width: 300px;
        float: left;
        margin-left: 40px;


    }
    .descricao {
        text-align: justify;
        margin-top: 15px;
    }
    .nome {
        color: #FF3432;
        margin-bottom: 15px;
        text-align: center;
    }
</style>
<h1>Pagina de carros</h1>
<p>
<form method="GET" action="index.php?">
    Desfrute dos mais belos carros da região. Pesquise pelo nome
    <input type="hidden" value="index" name="action" />
    <input type="hidden" value="carros" name="controller" />
    <input value="<?=array_key_exists("nome", $_REQUEST)?$_REQUEST['nome']:""?>" type="text" name="nome" />
</form>
</p>
<?php
if(isset($erro)){
    echo "<div class='alert'>$erro</div>";
}
?>
<div id="carlist">
    <?php
    foreach($resultado as $row){
    ?>

    <div class="car-item">
        <div class="nome">
            <?=$row["nome"]?>
        </div>
        <div class="foto">
           <img src="img/logo.png" />
        </div>
        <div class="descricao">
            <?=$row["descricao"]?>
        </div>
    </div>
    <?php }//Fechando foreach?>
</div>
<div style="clear: both"></div>
<article>
    <h2>Código fonte desta página</h2>
    <pre data-src="src/view/pages/carros.php" >
      <code  class="language-markup"></code>
    </pre>
</article>