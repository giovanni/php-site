<?php
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/15/13
 * Time: 3:31 PM 
 */
?>
<head>
    <h1>Bem vindo</h1>
    <p>Esta página é um demostrativo de tecnicas simples, com o intuito de ser utilizada didaticamente em curso de Introdução em PHP</p>
</head>

<article>
    <h2>Instruções</h2>
    <p>Para explorar a evolução da construção dessa página. Utilize o controle de versão GIT</p>
    <p>Clone o repositório com o comando</p>
    <pre>
        <code class="language-bash">
git clone https://giovanni@bitbucket.org/giovanni/php-site.git
        </code>
    </pre>
    <p>
        Uma pasta php-site será criada no diretório local. Mova-a para o Document Root do servidor web (linux /var/www)
        Sugiro ver o log com as alterações <span class="enfase">(git log)</span> e ver cada modificação uma por um. Isso pode ser feito da seguinte forma:

    </p>

    <pre>
 <code class="language-bash">
git log --oneline
git checkout d0ea3ef
</code>
    </pre>

    <p>Os comandos acima mostra o log de alterações e vai para a primeira alteração. Para cada alteração subsequente faça <span class="enfase">git checkout #id</span></p>
</article>
<article>
    <h2>Código fonte desta página</h2>
    <pre data-src="src/view/pages/index.php" >
      <code  class="language-markup"></code>
    </pre>
</article>