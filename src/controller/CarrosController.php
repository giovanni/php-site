<?php
require_once __DIR__ . "/../model/Carro.php";
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/16/13
 * Time: 3:15 PM 
 */
class CarrosController {
    public function indexAction(){
        $nome = array_key_exists("nome",$_REQUEST) ? $_REQUEST["nome"] : null;
        $model = new Carro();
        if(is_null($nome)){
            $resultado = $model->findAll();
        }else {
            $resultado = $model->findByNome($nome);
        }
        // O conteúdo do include será armazenado ao invez de enviado para saida padrão
        ob_start();
        include_once __DIR__ . "/../view/pages/carros.php";
        $output = ob_get_clean();
        return $output;
    }
}