<?php
/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/16/13
 * Time: 7:43 PM 
 */

class IndexController {
   public function indexAction(){
       ob_start();
       include_once __DIR__ . "/../view/pages/index.php";
       $retorno = ob_get_clean();
       return $retorno;
   }
} 