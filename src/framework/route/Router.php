<?php
/**
 * Chama as funções dos controladores
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/16/13
 * Time: 6:49 PM 
 */

class Router {
    static function resolveRoute(){
        $action = isset($_REQUEST["action"])? $_REQUEST["action"] : "index";
        $controller = isset($_REQUEST["controller"]) ? $_REQUEST["controller"] : "index";
        // Sempre as primeiras letras do nome sao maiusculas
        $action = ucfirst($action);
        $controller = ucfirst($controller);
        $fullControllerName = $controller . "Controller";
        $fullActionName = $action . "Action";
        require_once __DIR__ . "/../../controller/" . $fullControllerName . ".php" ;
        $instance = new $fullControllerName;
        return $instance->{$fullActionName}();
    }
} 