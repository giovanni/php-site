<?php
/*Mostrar todos os erros no PHP*/
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require_once __DIR__ . "/src/framework/route/Router.php";
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/layout.css" />
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- Syntaxe de codigo nas paginas http://prismjs.com/index.html  -->

        <link rel="stylesheet" href="js/vendor/prism.css" />

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div id="pagina">
            <header id="cabecalho">
               <div id="logo">
                   <img src="img/logo.png" />
               </div>
               <div id="logotext">
                   Procure seu carro de forma eficiente
               </div>
            </header>
            <nav id="menuPrincipal" class="horizontalMenu">
                <?php include "src/view/layout/menu.php" ?>
            </nav>
            <section id="conteudo">

                <?php
                /**
                 * Não utilize esse tipo de inclusao em seus codigos.
                 * Um hacker pode explorar para carregar outros arquivos do sistema, como por exemplo o arquivo /etc/passwd
                 * Outro bug é que se uma pagina for incluida mais de uma vez ela entrará em loop, e seu servidor vai consumir
                 * memória ram atoa (se não estiver bem configurado pode travar o servidor)
                 */

                 echo Router::resolveRoute();
                ?>

            </section>

            <footer id="rodape">
                
                <a rel="license" href="http://creativecommons.org/licenses/by/3.0/br/deed.pt_BR">
                    <img alt="Licença Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/br/88x31.png" /></a>
                <span id="autor">Autor: Giovanni Cândido da Silva giovanni@atende.info</span><br />
                Esta obra foi licenciada sob uma Licença <a rel="license" href="http://creativecommons.org/licenses/by/3.0/br/deed.pt_BR">Creative Commons Atribuição 3.0 Brasil</a>.
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <!-- Syntaxe de codigo nas paginas http://prismjs.com/index.html  -->
        <script src="js/vendor/prism.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
       <!-- <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>-->
    </body>
</html>
