Site simples em PHP
==================

Esta página é um demostrativo de técnicas simples, com o intuito de ser utilizada didaticamente em curso de Introdução em PHP. Será visto:

  - Como criar um template incluindo paginas php dinamicamente
  - Utilizar algumas técnicas de CSS para estilizar o documento
  - Conectar em banco de dados criar uma lista dinamica na tela e fazer um filtro simples 

## Instruções

Para explorar a evolução da construção dessa página. Utilize o controle de versão GIT

Clone o repositório com o comando

    git clone https://giovanni@bitbucket.org/giovanni/php-site.git

Uma pasta php-site será criada no diretório local. Mova-a para o Document Root do servidor web (linux /var/www) Sugiro ver o log com as alterações (git log) e ver cada modificação uma por um. Isso pode ser feito da seguinte forma:

    git log --oneline
    git checkout d0ea3ef

Os comandos acima mostra o log de alterações e vai para a primeira alteração. Para cada alteração subsequente faça *git checkout #id*